from atmega import *
import argparse
from time import sleep
import os

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", help="Port to connect to", dest="port")
args = parser.parse_args()

serial_cfg = {"dev": args.port,
              "baud": 9600}


def clear(): return os.system('clear')


def print_menu():  # Your menu design here
    print(30 * "-", "MENU", 30 * "-")
    print("1. Read led state")
    print("2. Read button state")
    print("3. Read tempreture")
    print("4. Turn led on")
    print("5. Turn led off")
    print("6. Exit")
    print(67 * "-")


if __name__ == "__main__":
    print("Running port {}".format(args.port))

    with connect(args.port, 9600) as ser:
        try:
            i = 0

            loop = True

            print_menu()  # Displays menu

            while loop:  # While loop which will keep going until loop = False

                sleep(1)
                clear()
                i += 1
                choice = str(i)
                print(choice)

                print_menu()  # Displays menu

                if choice == "1":
                    read_led(ser)
                    # You can add your code or functions here
                elif choice == "2":
                    read_button(ser)
                    # You can add your code or functions here
                elif choice == "3":
                    read_temp(ser)
                    # You can add your code or functions here
                elif choice == "4":
                    led1on(ser)
                    # You can add your code or functions here
                elif choice == "5":
                    led1off(ser)
                    # You can add your code or functions here
                elif choice == "6":
                    # You can add your code or functions here
                    loop = False  # This will make the while loop to end as not value of loop is set to False
                else:
                    # Any integer inputs other than values 1-5 we print an error message
                    input("Wrong option selection. Enter any key to try again..")

        except KeyboardInterrupt:
            print('interrupted!')
