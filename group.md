Group: TTT
-----------
Class: B

Members:

* Sebastian Mason sebski123
* Tihamer Biliboc tiha0006
* Thomas Bargisen N/A

Juniper router:

* Management SSH: 10.217.19.211:22
* External ip: 10.217.19.211

Raspberry:

* SSH access: 10.217.19.211:1100
* REST API access: [http://10.217.19.211:1102](http://10.217.19.211:1102)

Group web server:

* SSH access: 10.217.16.74:22
* REST API access: [http://10.217.16.74:5000](http://10.217.16.74:5000)

