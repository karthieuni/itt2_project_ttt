# Documentation

## Sensors
---------
### Schematic
![Sensor Schematic](/Sensors/TempSensorSchematic.PNG)
### Sensor Details  
Document containing basic information/guidance on the process behind making a veroboard to fit your needs.

**Calculations**

![Here](/Sensors/Led_resistor_calculation.png)

**Fritzing**

After the calculations we opened up Fritzing as instructed and started gathering the parts from the respective directories, we made a little Veroboard design based on the provided diagrams we were given by our Advisors, we hit save and moved to E-Lab.

**Breadboard tests**

After designing the circuit we moved to the E-Lab and began assembling the circuit, after which we used the basic DC power-supply and a multi-meter to check that everything worked as we intended.

![Here](/Sensors/##Led_off_bb.png)

![Here](/Sensors/Led_on_bb.png)

**Veroboard**

After finishing the breadboard test concluding that our circuit did indeed work as we planned it to we moved everything slightly more compacted onto a Veroboard and soldered everything in place.

![Here](/Sensors/Led_off_vb.png)

![Here](/Sensors/Led_on_vb.png)


## ATMega328
---------
### Schematic
![ATMega328p Schematic](/ATMega-328/ArduinoSchematic.PNG)

### ATMega328 Details  

Move [this](/arduino/Atmel_studio_solution/DataLogger/DataLogger/DataLogger.c) code to the atmega328p using AtmelStudio.

## Raspberry Pi
---------
### Schematic
![Raspberry Schematic](/Raspberry_Pi/RaspberrySchematic.png)

### Raspberry Pi Details
As the Raspberry Pi operates with 3.3V, and ATMega328p operates with 5V, a level shifter is needed between the two.

1. Install Raspian_stretch_lite using [this](https://drive.google.com/file/d/1H8NgczVzBk0rPzTo6zmltT2zIvfLon6n/view) image
1. Update the variables at the top of [this](/Raspberry_Pi\reeeinstall.sh) script to match the desired values
1. Transfer the altered script to a USB-stick. OBS! make sure the script is named ```reeeinstall.sh```.
1. Connect the RPi to the Internet using an ethernet cable.
1. Power on the RPi.
1. Plug USB-stick into the RPi.


## Juniper Router
---------



## Webserver
---------
