Status-LED Protocol
-------------------

| Behavior      | Meaning/Problem           |         Solution          |
| :-----------: |:------------------------: | :-----------------------: |
| Constant red  | No connection to sensor   | Call support              |
| Constant blue | Manual mode/OK            | Do nothing                |
| Constant green| Auto mode/OK              | Do nothing                |
| Blinking red  | No connection to internet | Call support              |
| Blinking blue | Manual mode/Low power     | Change/Charge batteries   |
| Blinking green| Auto mode/Low power       | Change/Charge batteries   |
| No light      | No power                  | Change/Charge batteries   |